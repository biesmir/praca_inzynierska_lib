#include <stdio.h>

void mprint(const float const *matrix, int h, int w)
{
    for(int i = 0; i<h; i++)
      {
      printf("| ");
	for(int j = 0; j<w; j++)
	  {
	    printf("%.10f ", *(matrix + i*w + j));
	  }
      printf("|\n");
      }
}
