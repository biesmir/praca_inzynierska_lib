#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../inc/matrix.h"

void dh_scara(const float q1, const float q2, const float q3, const float q4, const float a1,
	      const float a2, const float d1, float* K)
{
	*(K + 0) = cosf(q1+q2+q4);
	*(K + 1) = -sinf(q1+q2+q4);
	*(K + 2) = 0.0;
	*(K + 3) = a1*cosf(q1)+a2*cosf(q1+q2);

	*(K + 4) = sinf(q1+q2+q4);
	*(K + 5) = cosf(q1+q2+q4);
	*(K + 6) = 0.0;
	*(K + 7) = a1*sinf(q1) + a2*sinf(q1+q2);

	*(K + 8) = 0.0;
	*(K + 9) = 0.0;
	*(K + 10) = 1.0;
	*(K + 11) = d1+q3;

	*(K + 12) = 0.0;
	*(K + 13) = 0.0;
	*(K + 14) = 0.0;
	*(K + 15) = 1.0;

}

void dh_scara_double(const double q1, const double q2, const double q3, const double q4, const double a1,
	      const double a2, const double d1, double* K)
{
	*(K + 0) = cos(q1+q2+q4);
	*(K + 1) = -sin(q1+q2+q4);
	*(K + 2) = 0.0;
	*(K + 3) = a1*cos(q1)+a2*cos(q1+q2);

	*(K + 4) = sin(q1+q2+q4);
	*(K + 5) = cos(q1+q2+q4);
	*(K + 6) = 0.0;
	*(K + 7) = a1*sin(q1) + a2*sin(q1+q2);

	*(K + 8) = 0.0;
	*(K + 9) = 0.0;
	*(K + 10) = 1.0;
	*(K + 11) = d1+q3;

	*(K + 12) = 0.0;
	*(K + 13) = 0.0;
	*(K + 14) = 0.0;
	*(K + 15) = 1.0;

}

void exp_scara_basic(const float q1, const float q2, const float q3, const float q4, const float a1,
	      const float a2, const float d1, float* K)
{

  const float K0[16] = {1.0, 0.0, 0.0, a1+a2,
		  0.0, 1.0, 0.0, 0.0,
		  0.0, 0.0, 1.0, d1,
		  0.0, 0.0, 0.0, 1.0};
    //mprint(K0,4,4);
  const float dz1[16] = {0, -q1, 0, 0,
		    q1, 0, 0, 0,
		    0, 0, 0, 0,
		    0, 0, 0, 0};
  
  const float dz2[16] = {0, -q2, 0, 0,
			 q2, 0, 0, -a1*q2,
			 0, 0, 0, 0,
			 0, 0, 0, 0};

  const float dz3[16] = {0, 0, 0, 0,
			 0, 0, 0, 0,
			 0, 0, 0, q3,
			 0, 0, 0, 0};

  const float dz4[16] = {0, -q4, 0, 0,
			 q4, 0, 0, -(a1+a2)*q4,
			 0, 0, 0, 0,
			 0, 0, 0, 0}; 

  float tmp1[16];
  float tmp2[16];  
  float exp1[16];
  float exp2[16];
  float exp3[16];
  float exp4[16];  
  matrix_exp_basic(dz1, exp1);
  matrix_exp_basic(dz2, exp2);  
  matrix_exp_basic(dz3, exp3);
  matrix_exp_basic(dz4, exp4);  

  /*   printf("dz1\n"); */
  /*   mprint(dz1,4,4); */
  /*   printf("dz2\n"); */
  /*   mprint(dz2,4,4); */
  /*   printf("dz3\n"); */
  /*   mprint(dz3,4,4); */
  /*   printf("dz4\n"); */
  /*   mprint(dz4,4,4);     */
  
  /* printf("exp1\n"); */
  /*   mprint(exp1,4,4); */
  /* printf("exp2\n"); */
  /*   mprint(exp2,4,4); */
  /* printf("exp3\n"); */
  /*   mprint(exp3,4,4); */
  /* printf("exp4\n"); */
  /*   mprint(exp4,4,4);     */
  /* printf("K0\n"); */
  /*   mprint(K0,4,4);     */

  matrix4x4_mul(exp4, K0, tmp1);    
  matrix4x4_mul(exp3, tmp1, tmp2);
  matrix4x4_mul(exp2, tmp2, tmp1);  
  matrix4x4_mul(exp1, tmp1, K);
  
}
