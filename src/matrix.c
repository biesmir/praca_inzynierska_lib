#include "../inc/matrix.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


const float I[] = {1, 0, 0, 0,
		   0, 1, 0, 0,
		   0, 0, 1, 0,
		   0, 0, 0, 1};

inline static void swap(float **ptr1, float **ptr2)
{
  float *tmp_ptr = *ptr1;
  *ptr1 = *ptr2;
  *ptr2 = tmp_ptr;
}

int matrix3x3_mul(const float matrix1[9], const float matrix2[9], float matrix3[9])
{
  for(int i=0; i<3;i++)
    {
      for(int j=0; j<3;j++)
      	{
      	  matrix3[i*3+j] = matrix1[i*3+0]*matrix2[0+j] + matrix1[i*3+1]*matrix2[3+j] + matrix1[i*3+2]*matrix2[6+j];
      	}
    } 
  return 0;
}


void matrix4x4_mul(const float matrix1[16], const float matrix2[16], float matrix_res[16])
{
  for(int i=0; i<4;i++)
    {
      for(int j=0; j<4;j++)
      	{
      	  matrix_res[i*4+j] = matrix1[i*4+0]*matrix2[0+j] + matrix1[i*4+1]*matrix2[4+j] + matrix1[i*4+2]*matrix2[8+j] + matrix1[i*4+3]*matrix2[12+j];
      	}
    } 
}

void matrix4x4_mul_float(float *matrix, float val)
{
  for(int i=0; i<16;i++)
    *(matrix+i) *= val;
}

void matrix_add(const float *matrix1, const float *matrix2, float *matrix_res)
{
  for(int i=0; i<16;i++)
    *(matrix_res+i) = *(matrix1+i) + *(matrix2+i);

}

void matrix_power(const float *matrix, float *matrix_res, int num)
{
  float tmp1[16];
  float tmp2[16];
  float tmp[16];
  memcpy(tmp1, matrix, 64);  
  for(int i=1; i<num; i++)
    {
      matrix4x4_mul(tmp1, matrix, tmp2);
      memcpy(tmp, tmp1, 64);
      memcpy(tmp1, tmp2, 64);
      memcpy(tmp2, tmp, 64);
      //      swap(&tmp1, &tmp2);
    }
  memcpy(matrix_res, tmp1, 64);
  
}
//#define OEXP
#define STEPS 36
void matrix_exp_basic(const float *matrix, float *matrix_res)
{
  float tmp[16];
  float tmp_sum[16];
  float n = 1;
  
      #ifdef OEXP
    float tmp_exp[16];
  memcpy(tmp_exp, I, 64);
      #endif

  memcpy(tmp_sum, I, 64);
  for(int i=1;i<STEPS;i++)
    {
      n*=i;

      #ifdef OEXP
      matrix4x4_mul(matrix, tmp_exp, tmp);
      memcpy(tmp_exp, tmp, 64);
      #else
      matrix_power(matrix, tmp, i);
      #endif

      matrix4x4_mul_float(tmp, (float)(1.0/n));
      matrix_add(tmp_sum, tmp, tmp_sum);
      	/* printf("\ti=%d   class=%d\n", i, fpclassify(tmp_sum[1])); */
    }
  memcpy(matrix_res, tmp_sum, 64);
}
