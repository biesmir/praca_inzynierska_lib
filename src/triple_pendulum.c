#include <math.h>
#include <stdlib.h>

void dh_tp(float q1, float q2, float q3, float l1, float l2, float l3, float* K)
{
	*(K + 0) = cosf(q1)*cosf(q2)*cosf(q3);
	*(K + 1) = -sinf(q1)*sinf(q2)*sinf(q3);
	*(K + 2) = 0.0;
	*(K + 3) = l1*cosf(q1)+l2*cosf(q1)*cosf(q2)+l3*cosf(q1)*cosf(q2)*cosf(q3);

	*(K + 4) = sinf(q1)*sinf(q2)*sinf(q3);
	*(K + 5) = cosf(q1)*cosf(q2)*cosf(q3);
	*(K + 6) = 0.0;
	*(K + 7) = l1*sinf(q1)+l2*sinf(q1)*sinf(q2)+l3*sinf(q1)*sinf(q2)*sinf(q3);

	*(K + 8) = 0.0;
	*(K + 9) = 0.0;
	*(K + 10) = 1.0;
	*(K + 11) = 0.0;

	*(K + 12) = 0.0;
	*(K + 13) = 0.0;
	*(K + 14) = 0.0;
	*(K + 15) = 1.0;

}
