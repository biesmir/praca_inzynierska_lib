void dh_edda(float q1, float q2, float l1, float l2, float* K);

void exp_edda_basic(float q1, float q2, float l1, float l2, float *K);

void edda_inv(float *q1, float *q2, const float l1, const float l2, const float K[16]);
