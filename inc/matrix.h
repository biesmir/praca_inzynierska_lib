#ifndef MATRIX_H
#define MARIX_H

int matrix3x3_mul(const float matrix1[9], const float matrix2[9], float matrix3[9]);

void matrix4x4_mul(const float matrix1[16], const float matrix2[16], float matrix3[16]);

void matrix4x4_mul_float(float *matrix, float val);

void matrix_add(const float *matrix1, const float *matrix2, float *matrix3);

void matrix_power(const float *matrix, float *matrix_out, int num);

void matrix_exp_basic(const float *matrix, float *matrix_out);


#endif //MARIX_H
