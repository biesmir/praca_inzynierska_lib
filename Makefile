CC=arm-none-eabi-gcc
AR=arm-none-eabi-ar
SRC=src
OBJ=obj
CFLAGS  = -g -O2 -Wall -Tstm32_flash.ld --specs=nosys.specs
CFLAGS += -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16 #-u_printf_float


lib: matrix.o double_pendulum.o mprint.o scara.o
	$(AR) rcs libinz.a $(OBJ)/matrix.o $(OBJ)/double_pendulum.o\
	 $(OBJ)/mprint.o $(OBJ)/scara.o

matrix.o:
	$(CC) $(CFLAGS) -c -o $(OBJ)/matrix.o $(SRC)/matrix.c

double_pendulum.o:
	$(CC) $(CFLAGS) -c -o $(OBJ)/double_pendulum.o $(SRC)/double_pendulum.c

mprint.o:
	$(CC) $(CFLAGS) -c -o $(OBJ)/mprint.o $(SRC)/mprint.c

scara.o:
	$(CC) $(CFLAGS) -c -o $(OBJ)/scara.o $(SRC)/scara.c

clean:
	rm -f $(OBJ)/*
	rm -f inz_lib.a
